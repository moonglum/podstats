# frozen_string_literal: true

require_relative "lib/podstats/version"

Gem::Specification.new do |spec|
  spec.name = "podstats"
  spec.version = Podstats::VERSION
  spec.authors = ["Lucas Dohmen"]
  spec.email = ["lucas@dohmen.io"]

  spec.summary = "Get statistics for the podcasts you subscribe to"
  spec.description = "Provide an OPML file, get statistics like the number of episodes this year"
  spec.homepage = "https://codeberg.org/moonglum/podstats"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://codeberg.org/moonglum/podstats"
  spec.metadata["changelog_uri"] = "https://codeberg.org/moonglum/podstats/src/branch/main/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) || f.start_with?(*%w[bin/ test/ spec/ features/ .git .circleci appveyor])
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "nokogiri", "~> 1.15"
  spec.add_dependency "feedjira", "~> 3.2"
  spec.add_dependency "http", "~> 5.1"
end
