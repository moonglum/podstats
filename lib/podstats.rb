# frozen_string_literal: true

require_relative "podstats/version"
require "nokogiri"
require "http"
require "feedjira"

module Podstats
  class Error < StandardError; end

  Feed = Struct.new(:name, :url)
  Episode = Struct.new(:title, :duration)

  class OpmlParser
    def parse(contents)
      outlines_in(contents).map do |outline|
        Feed.new(
          extract_name(outline.attributes),
          extract_url(outline.attributes)
        )
      end
    end

    private

    def outlines_in(contents)
      Nokogiri.XML(contents).xpath("//body/outline")
    end

    def extract_name(attributes)
      (attributes["title"] || attributes["text"]).value
    end

    def extract_url(attributes)
      attributes["xmlUrl"].value
    end
  end

  class FeedAnalyzer
    def analyze(url)
      feed = HTTP.get(url).to_s

      Feedjira.parse(feed).entries.find_all do |entry|
        entry.published.year == 2023
      end.map do |entry|
        Episode.new(entry.title, entry["itunes_duration"])
      end
    end
  end
end
